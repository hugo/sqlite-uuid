.load sqlite-uuid
SELECT uuid();


.timer on
WITH gen AS (SELECT 0 AS num UNION ALL SELECT num + 1 FROM gen WHERE num + 1 < 10000)
SELECT num, uuid() FROM gen;
