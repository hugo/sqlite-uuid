.PHONY: all clean install

libs = sqlite3 uuid
CFLAGS = -Wall -pedantic -std=c2x -fPIC \
		 -ggdb \
		 $(shell pkg-config --cflags $(libs))
LDLIBS = $(shell pkg-config --libs $(libs))

C_FILES = sqlite-uuid.c
O_FILES = $(C_FILES:%.c=%.o)

DESTDIR = /
PREFIX = /usr/local

all: sqlite-uuid.so

sqlite-uuid.so: $(O_FILES)
	$(CC) -shared -o $@ $^ $(LDLIBS)

install: all
	install -D -t $(DESTDIR)/$(PREFIX)/lib sqlite-uuid.so

clean:
	-rm $(O_FILES)
	-rm sqlite-uuid.so
