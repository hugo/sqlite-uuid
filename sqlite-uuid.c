#include <sqlite3ext.h>
SQLITE_EXTENSION_INIT1
#include <uuid.h>

#define UUID_BYTES (128/8)
#define UUID_STRLEN (UUID_BYTES * 2 + 4)


static void sqlite_uuid(sqlite3_context *context, int argc, sqlite3_value **argv)  {
	uuid_t uuid;
	uuid_generate_random(uuid);

	char str[UUID_STRLEN + 1];
	uuid_unparse(uuid, &str[0]);

	sqlite3_result_text(context, str, UUID_STRLEN, SQLITE_TRANSIENT);

	uuid_clear(uuid);
}

static void sqlite_uuid_blob(sqlite3_context *context, int argc, sqlite3_value **argv) {
	uuid_t uuid;
	uuid_generate_random(uuid);

	sqlite3_result_blob(context, uuid, UUID_BYTES, SQLITE_TRANSIENT);

	uuid_clear(uuid);
}

#ifdef _WIN32
__declspec(dllexport)
#endif
int sqlite3_extension_init (
	sqlite3 *db,
	char **pzErrMsg,
	const sqlite3_api_routines *pApi
) {
	int rc = SQLITE_OK;
	SQLITE_EXTENSION_INIT2(pApi);

	(void) pzErrMsg;

	rc = sqlite3_create_function(db, "uuid", 0,
			SQLITE_UTF8|SQLITE_INNOCUOUS/*|SQLITE_DIRECTONLY*/,
			0, sqlite_uuid, 0, 0);

	if (rc != SQLITE_OK) return rc;

	rc = sqlite3_create_function(db, "uuid-blob", 0,
			SQLITE_UTF8|SQLITE_INNOCUOUS/*|SQLITE_DIRECTONLY*/,
			0, sqlite_uuid_blob, 0, 0);

	return rc;
}
